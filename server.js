/* eslint-disable no-undef */
const express = require('express');
const next = require('next');
const LRUCache = require('lru-cache');

// This is where we cache our rendered HTML pages
const ssrCache = new LRUCache({
  max:
    100 *
    1024 *
    1024 /* cache size will be 100 MB using `return n.length` as length() function */,
  length: function (n, key) {
    return n.length;
  },
  maxAge: 1000 * 60 * 60 * 24 * 30,
});

const dev = process.env.NODE_ENV !== 'production';
console.log('dev', dev);
const nextApp = next({
  dev,
});
const handle = nextApp.getRequestHandler();
nextApp
  .prepare()
  .then(() => {
    const server = express();
    const port = 3030;

    const serviceWorkers = [
      {
        filename: 'firebase-messaging-sw.js',
        path: './public/firebase-messaging-sw.js',
      },
    ];
    serviceWorkers.forEach(({ filename, path }) => {
      server.get(`/${filename}`, (req, res) => {
        nextApp.serveStatic(req, res, path);
      });
    });

    server.get('/_next/*', (req, res) => {
      /* serving _next static content using next.js handler */
      handle(req, res);
    });

    server.get('*', (req, res) => {
      /* serving page */
      return handle(req, res);
      // return renderAndCache(req, res);
    });

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`>> Ready on  ${port}`);
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });

/*
 * NB: make sure to modify this to take into account anything that should trigger
 * an immediate page change (e.g a locale stored in req.session)
 */
function getCacheKey(req) {
  return `${req.path}`;
}

async function renderAndCache(req, res) {
  const key = getCacheKey(req);

  // If we have a page in the cache, let's serve it
  if (ssrCache.has(key)) {
    //console.log(`serving from cache ${key}`);
    res.setHeader('x-cache', 'HIT');
    res.send(ssrCache.get(key));
    return;
  }

  try {
    //console.log(`key ${key} not found, rendering`);
    // If not let's render the page into HTML
    const html = await nextApp.renderToHTML(req, res, req.path, req.query);

    // Something is wrong with the request, let's skip the cache
    if (res.statusCode !== 200) {
      res.send(html);
      return;
    }

    // Let's cache this page
    ssrCache.set(key, html);

    res.setHeader('x-cache', 'MISS');
    res.send(html);
  } catch (err) {
    nextApp.renderError(err, req, res, req.path, req.query);
  }
}
