import React from 'react';
import InputField from '../components/InputField';
import ImgBg from '../assets/background.png';
import ImgLogo from '../assets/logo-umaha.svg';
import { BERANDA , RESET} from '../helper/routes';
import { BASE_API_URL} from '../helper/globals';
import Router, { useRouter } from 'next/router';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Image, Button, Checkbox, message } from 'antd';
import { UserOutlined, UnlockOutlined, WhatsAppOutlined  } from '@ant-design/icons';
import { useSelector, useDispatch } from 'react-redux';
import { setAccessToken } from '../store/actionCreators';

const backgroundImg = {
    backgroundImage: 'url(' + ImgBg.src + ')',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};

const titleFont = {
    fontSize: '18px',
    color: '#3A8919',
    marginBottom: 0,
};

const verticalLine = {
    width: '2px',
    borderLeft: '1px solid #3A8919',
    height: '85px',  
    marginLeft: '10px',
    marginRight: '10px',
};

const titleWrapper = {
    paddingTop: '12px',
};

const inputLabel = {
    fontSize: '14px',
    letterSpacing: '1px',
    marginBottom: '10px',
};

const linkLabel = {
    fontSize: '14px',
    letterSpacing: '1px',
    color: '#3A8919',
    cursor: 'pointer',
};

const iconStyle = {
    fontSize: '40px',
    marginLeft: '10px',
    color: '#3A8919',
}

const iconLink = {
    fontSize: '30px',
    color: '#3A8919',
}

const aStyle = {
    fontSize: '14px',
    letterSpacing: '1px',
    color: '#3A8919',
    cursor: 'pointer',
    textDecoration: 'underline',
};

const btnStyle = {
    background: "#3A8A19", 
    borderColor: "#3A8A19", 
    width: '100%', 
    borderRadius: '8px',
    color: '#fff',
};

message.config({
    getContainer: () => document.getElementById('container'),
    top: 40,
  });
  

const FormSchema = Yup.object().shape({
    username: Yup.string().required('Username is required').min(7, 'Your username must be at least 7 characters.'),
    password: Yup.string().required('Password is required.'),
});

function HomePage() {
    const dispacth = useDispatch();
    const formik = useFormik({
        initialValues: {
          username: '',
          password: '',
        },
        validationSchema: FormSchema,
    });

    const router = useRouter();

    const handleSubmit = async () => {
        if (Object.keys(formik.errors).length > 0) return;
        const { values } =  formik;
        const password = values.password;
        const username = values.username;
        const res = await fetch( `${BASE_API_URL}/auth/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        });

        const data = await res.json();

        if (data.message == "OK") {
            message.success('Login Success');
            setTimeout(() => {
                Router.push(BERANDA);
              }, 500);
            const token = data.data.token;
            // delete data.data.token;
            dispacth(setAccessToken(token));
        } else {
            message.error('Failed Login');
        }
        
    }

    const handleResetPassword = () => {
        router.push(RESET)
    }

    const onChange = (e) => {
        console.log(`checked = ${e.target.checked}`);
    };
    

    return <div className="grid place-items-center h-screen" style={backgroundImg}>
            <div className="antd-ns antd-styled" id="container"></div>
                <div className="bg-stone-200 p-10 rounded-2xl bg-opacity-80">
                    <div className="flex">
                        <Image width={85} className="pb-5" src={ImgLogo.src} />
                        <div style={verticalLine} />
                        <div className='text-left' style={titleWrapper}>
                            <p className="font-600" style={titleFont}>SISTEM LANGITAN</p>
                            <p className="font-600" style={titleFont}>UNIVERSITAS MAARIF HASYIM LATIF </p>
                        </div>
                    </div>
                    <div className="pl-5 pr-5">
                            <div className="mb-6">
                                <div style={inputLabel}>Username</div>
                                <div className="flex">
                                    <InputField 
                                        name="username"
                                        type="text" 
                                        placeholder={'NIM / NIDN / NIP / NITK'} 
                                        isRequired
                                        value={formik.values.username} 
                                        onChange={formik.handleChange}
                                        error={formik.errors.username}
                                    />
                                    <UserOutlined style={iconStyle}/>
                                </div>
                            </div>
                            <div className="mb-6">
                                <div style={inputLabel}>Password</div>
                                <div className="flex">
                                    <InputField 
                                        type="password" 
                                        placeholder={'Input password'}  
                                        isRequired
                                        isPassword
                                        name="password"
                                        value={formik.values.password} 
                                        onChange={formik.handleChange}
                                        error={formik.errors.password}
                                    />
                                    <UnlockOutlined className="ml-7" style={iconStyle}/>
                                </div>
                            </div>
                            <div className="mb-6 flex justify-between">
                                <Checkbox onChange={onChange}>
                                    <div style={inputLabel}>Remember me</div>
                                </Checkbox>
                                <div style={linkLabel} onClick={handleResetPassword}>Reset password</div>
                            </div>
                            <div className="mb-6">
                                <Button size={'large'} style={btnStyle} type="submit" onClick={handleSubmit} >Login</Button>
                                
                            </div>
                        <div className="mb-6 text-center">
                            <div style={inputLabel}>Jika perlu bantuan, hubungi kami di :</div>
                        </div>
                        <div className="text-center">
                            <WhatsAppOutlined style={iconLink} />
                            <a href="https://web.whatsapp.com/" className="ml-2" style={aStyle}>Helpdesk</a>
                        </div>
                    </div>
                </div>
           </div>
}

export default HomePage