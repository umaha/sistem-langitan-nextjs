// import Button from '../components/Button';
import InputField from '../../components/InputField';
import ImgBg from '../../assets/background.png';
import ImgLogo from '../../assets/logo-umaha.svg';
import { LOGIN } from '../../helper/routes';
import Router, { useRouter } from 'next/router';
import { Image, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';

const backgroundImg = {
    backgroundImage: 'url(' + ImgBg.src + ')',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};

const titleFont = {
    fontSize: '18px',
    color: '#3A8919',
    marginBottom: 0,
};

const verticalLine = {
    width: '2px',
    borderLeft: '1px solid #3A8919',
    height: '85px',  
    marginLeft: '10px',
    marginRight: '10px',
};

const titleWrapper = {
    paddingTop: '12px',
};

function RegisterPage() {
    const router = useRouter();

    const handleRedirect = () => {
        // console.log('test');
        router.push(LOGIN);
    }
    

    return <div className="grid place-items-center h-screen" style={backgroundImg}>
                <div className="text-center bg-stone-200 p-10 rounded-lg bg-opacity-90">
                    <div className="flex">
                        <Image width={85} className="pb-5" src={ImgLogo.src} />
                        <div style={verticalLine} />
                        <div className='text-left' style={titleWrapper}>
                            <p className="font-600" style={titleFont}>SISTEM LANGITAN</p>
                            <p className="font-600" style={titleFont}>UNIVERSITAS MAARIF HASYIM LATIF </p>
                        </div>
                    </div>
                    <div className="mb-5">
                        <InputField type="text" placeholder={'Username'} icon={<UserOutlined/>} />
                    </div>
                    <div className="mb-5">
                        <InputField type="text" placeholder={'Email'} icon={<UserOutlined/>} />
                    </div>
                    <div className="mb-5">
                        <InputField type="password" placeholder={'Password'} />
                    </div>
                    <div className="mb-5">
                        <InputField type="password" placeholder={'Confirm Password'} />
                    </div>
                    <div className="mb-5">
                        <Button type="primary" shape="round" size={'large'} style={{ background: "#3A8A19", borderColor: "#3A8A19", width: '100%' }}>Register</Button>
                    </div>
                    <div >
                        <Button type="primary" shape="round" size={'large'} style={{ color: "#3A8A19", background: "transparent", borderColor: "transparent"}} onClick={handleRedirect}>Back</Button>
                    </div>
                </div>
           </div>
}

export default RegisterPage