// import Button from '../../components/Button';
import InputField from '../../components/InputField';
import ImgBg from '../../assets/background.png';
import ImgLogo from '../../assets/logo-umaha.svg';
import { MHS, REGISTER } from '../../helper/routes';
import Router, { useRouter } from 'next/router';
import { Image, Button, Anchor } from 'antd';
import { UserOutlined, CalendarOutlined, WhatsAppOutlined } from '@ant-design/icons';

const { Link } = Anchor;

const backgroundImg = {
    backgroundImage: 'url(' + ImgBg.src + ')',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};

const titleFont = {
    fontSize: '18px',
    color: '#3A8919',
    marginBottom: 0,
};

const titleHeader = {
    fontSize: '18px',
    color: '#fff',
    marginBottom: 0,
};

const verticalLine = {
    width: '2px',
    borderLeft: '1px solid #3A8919',
    height: '85px',  
    marginLeft: '10px',
    marginRight: '10px',
};

const titleWrapper = {
    paddingTop: '12px',
};

const inputLabel = {
    fontSize: '14px',
    letterSpacing: '1px',
    marginBottom: '10px',
};

const infoLabel = {
    fontSize: '14px',
    letterSpacing: '1px',
    marginBottom: '10px',
    color: 'grey',
    textAlign: 'justify'
};

const infoLabelRed = {
    fontSize: '14px',
    letterSpacing: '1px',
    marginBottom: '10px',
    color: 'red',
    textAlign: 'justify'
};

const linkLabel = {
    fontSize: '14px',
    letterSpacing: '1px',
    color: '#3A8919',
    cursor: 'pointer',
};

const iconStyle = {
    fontSize: '40px',
    marginLeft: '10px',
    color: '#3A8919',
}

const wrapperHeader = {
    background: '#3A8919',
}


function HomePage() {
    const router = useRouter();

    const handleRedirect = () => {
        router.push('/');
    }

    const handleRedirectRegister = () => {
        router.push(REGISTER);
    }

    const onChange = (e) => {
        console.log(`checked = ${e.target.checked}`);
    };

    return <div className="grid place-items-center h-screen" style={backgroundImg}>
                <div className="bg-stone-200 rounded-2xl bg-opacity-80" style={{ maxWidth: '485px'}}>
                    <div className="flex pt-10 pl-10 pr-10 pb-3">
                        <Image width={85} className="pb-5" src={ImgLogo.src} />
                        <div style={verticalLine} />
                        <div className='text-left' style={titleWrapper}>
                            <p className="font-600" style={titleFont}>SISTEM LANGITAN</p>
                            <p className="font-600" style={titleFont}>UNIVERSITAS MAARIF HASYIM LATIF </p>
                        </div>
                    </div>
                    <div>
                        <div className='text-center' style={wrapperHeader}>
                            <p className="font-600 p-2" style={titleHeader}>RESET PASSWORD</p>
                        </div>
                    </div>
                    <div className="pr-16 pl-16 pt-8 pb-8">
                        <div className="mb-6">
                            <div style={inputLabel}>Username</div>
                            <div className="flex">
                                <InputField type="text" placeholder={'NIM / NIDN / NIP / NITK'} />
                                <UserOutlined style={iconStyle}/>
                            </div>
                        </div>
                        <div className="mb-6">
                            <div style={inputLabel}>Tanggal Lahir</div>
                            <div className="flex">
                                <InputField type="date" placeholder={'dd-mm-yyyy'} />
                                <CalendarOutlined className="ml-7" style={iconStyle}/>
                            </div>
                        </div>
                        <div className="mb-6 flex justify-between">
                            <Button size={'large'} style={{ background: "transparent", borderColor: "#3A8A19", width: '125px', borderRadius: '8px', color: "#3A8A19" }} onClick={handleRedirect}>Back</Button>
                            <Button type="primary" size={'large'} style={{ background: "#3A8A19", borderColor: "#3A8A19", width: '125px', borderRadius: '8px' }} onClick={handleRedirect}>Reset</Button>
                        </div>
                        <div className="mb-5">
                            <div style={infoLabel}>Link reset dikirim ke email yang tercatat pada data langitan anda.</div>
                        </div>
                        <div className="mb-6">
                            <div style={infoLabelRed}>*Apabila link tidak terkirim / email tidak valid silahkan hubungi helpdesk.</div>
                        </div>
                    </div>
                </div>
           </div>
}

export default HomePage