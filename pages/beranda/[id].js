import { useRouter } from "next/router"

export default function Mhs() {
    
    const router = useRouter();
    const { id } = router.query;

    return <h1>mahasiswa {id}</h1>
}