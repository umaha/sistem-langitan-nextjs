import React, { useState, useEffect } from 'react';
import MainLayout from '../../components/MainLayout';
import { Table } from 'antd';
import { Line } from '@ant-design/plots';
import { useSelector } from 'react-redux';

const { Column } = Table;

const data = [
    {
      year: '1991',
      value: 3,
    },
    {
      year: '1992',
      value: 4,
    },
    {
      year: '1993',
      value: 3.5,
    },
    {
      year: '1994',
      value: 5,
    },
    {
      year: '1995',
      value: 4.9,
    },
    {
      year: '1996',
      value: 6,
    },
    {
      year: '1997',
      value: 7,
    },
    {
      year: '1998',
      value: 9,
    },
    {
      year: '1999',
      value: 5,
    },
  ];

  const config = {
    data,
    xField: 'year',
    yField: 'value',
    label: {},
    point: {
      size: 5,
      shape: 'diamond',
      style: {
        fill: 'white',
        stroke: '#5B8FF9',
        lineWidth: 2,
      },
    },
    tooltip: {
      showMarkers: false,
    },
    state: {
      active: {
        style: {
          shadowBlur: 4,
          stroke: '#000',
          fill: 'red',
        },
      },
    },
    interactions: [
      {
        type: 'marker-active',
      },
    ],
  };

const Panel = {
    background: '#fff',
    width: '100%',
    borderRadius: '8px',
    padding: '32px'
}

const HeaderTitle = {
    fontSize: '16px',
    color: '#3A8919',
    marginBottom: 0,
    fontWeight: 700,
}

const dataTagihanSource = [
    {
        key: '1',
        semester: '2018/2019 Genap',
        tagihan: 'Rp 2.000.000'
    },
    {
        key: '2',
        semester: '2019/2020 Ganjil',
        tagihan: 'Rp 3.000.000'
    },
    {
        key: '3',
        semester: '2019/2020 Genap',
        tagihan: 'Rp 4.000.000'
    },
    {
        key: '',
        semester: '',
        tagihan: 'Rp 9.000.000'
    },
];

const columnsTagihan = [
    {
      title: 'No',
      dataIndex: 'key',
      key: 'key',
    },
    {
      title: 'Semester',
      dataIndex: 'semester',
      key: 'semester',
    },
    {
      title: 'Tagihan',
      dataIndex: 'tagihan',
      key: 'tagihan',
    },
];

const dataSource = [
    {
      key: '1',
      name: 'Animasi 3D',
      jadwal: 'Senin, 18:00 - 21:00',
      ruang: 'B 202',
      dosen: 'Moh Cholisatur Rizaq S.sn, M.Sn.'
    },
    {
      key: '2',
      name: 'Desain Grafis',
      jadwal: 'Senin, 18:00 - 21:00',
      ruang: 'B 202',
      dosen: 'Moh Cholisatur Rizaq S.sn, M.Sn.'
    },
];
  
const columns = [
    {
      title: 'No',
      dataIndex: 'key',
      key: 'key',
    },
    {
      title: 'Mata Kuliah',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Jadwal Perkuliahan',
      dataIndex: 'jadwal',
      key: 'jadwal',
    },
    {
      title: 'Ruang',
      dataIndex: 'ruang',
      key: 'ruang',
    },
    {
      title: 'Dosen Pengampu',
      dataIndex: 'dosen',
      key: 'dosen',
    },
];

const Beranda = () => {
    const accessToken = useSelector((state) => state.accessToken);
    // console.log(accessToken);


    return (
        <MainLayout>
           <div style={Panel}>
               <div style={HeaderTitle}>
                    Grafik Indeks PrestasiSemester (IPS)
               </div>
               <div className='mb-10'/>
               <Line style={{ height: '300px !important'}} {...config} />
               <div className='mb-10'/>
               <div style={HeaderTitle}>
                    Tagihan Belum Terbayar
               </div>
               <div className='mb-5'/>
               <Table className='w-6/12' pagination={false} dataSource={dataTagihanSource} columns={columnsTagihan} />
               <div className='mb-10'/>
               <div style={HeaderTitle}>
                    Jadwal Perkuliahan
               </div>
               <div className='mb-5'/>
               <Table pagination={false} dataSource={dataSource} columns={columns} />
           </div>
        </MainLayout>
    )
}

export default Beranda;