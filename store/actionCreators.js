import {
    SET_USER,
    SET_ACCESS_TOKEN
} from './actions';

export const setUser = (payload) => ({
    type: SET_USER,
    payload,
});

export const setAccessToken = (payload) => ({
    type: SET_ACCESS_TOKEN,
    payload,
});