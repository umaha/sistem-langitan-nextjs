import { Button, Link } from 'antd';

const Btn = ({ text }) => { 
    return (
        <Button type="primary" shape="round" size={'large'} style={{ background: "#3A8A19", borderColor: "#3A8A19", width: '100%' }}>{text}</Button>
    );
 }

 export default Btn;