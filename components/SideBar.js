import {
    DesktopOutlined,
    FileOutlined,
    AppstoreOutlined,
    TeamOutlined,
    UserOutlined,
    RiseOutlined,
    ProfileOutlined,
    UsergroupAddOutlined,
    InfoCircleOutlined,
    ReconciliationOutlined,
    BankOutlined
  } from '@ant-design/icons';
import { Button, Layout, Menu, Image } from 'antd';
import  Router, { useRouter} from 'next/router';
import React, { useState } from 'react';
import ImgLogo from '../assets/logo-umaha.svg';



  const { Header, Content, Footer, Sider } = Layout;

  const logoWrapper = {
    padding: '20px',
    display: 'flex',
    justifyContent: 'center'
  };

  const titleFont = {
    fontSize: '12px',
    color: '#3A8919',
    marginBottom: 0,
    fontWeight: 700,
};

const verticalLine = {
    width: '2px',
    borderLeft: '1px solid #3A8919',
    height: '55px',  
    marginLeft: '5px',
    marginRight: '5px',
};

const titleWrapper = {
    paddingTop: '10px',
};

const btnStyle = {
  background: "#3A8A19", 
  borderColor: "#3A8A19", 
  width: '70%', 
  borderRadius: '8px',
  color: '#fff',
};

const btnWrapper = {
  width: '100%',
  textAlign: 'center',
  padding: '20px'
};
  
  function getItem(label, key, icon, children) {
    return {
      key,
      icon,
      children,
      label,
    };
  }
  
  const items = [
    getItem('Beranda', '1', <AppstoreOutlined />),
    getItem('Biodata', 'sub1', <UserOutlined />, [
        getItem('Ubah Biodata', '2'),
        getItem('Password', '3'),
        getItem('Info', '4'),
    ]),
    getItem('Akademik', 'sub2', <RiseOutlined />, [
        getItem('Kalender Akademik', '5'),
        getItem('Peserta Mata Ajar', '6'),
        getItem('Kartu Hasil Studi', '7'),
        getItem('Kartu Rencana Studi', '20'),
        getItem('KPRS', '21'),
        getItem('Jadwal Ujian', '22'),
        getItem('Histori Nilai', '23'),
        getItem('Rekap Absen', '24'),
        getItem('Jadwal Kuliah', '25'),
        getItem('Status Akademik', '26'),
        getItem('Draft Ijazah', '27'),
        getItem('Kuliah Kerja Nyata (KKN)', '28'),
        getItem('Pengambilan Semester Pendek', '29'),
    ]),
    getItem('Dosen', 'sub3', <TeamOutlined />, [
        getItem('Team 1', '8'), 
        getItem('Team 2', '9')
    ]),
    getItem('Keuangan', 'sub4', <BankOutlined />, [
        getItem('Team 1', '10'), 
        getItem('Team 2', '11')
    ]),
    getItem('Evaluasi', 'sub5', <ProfileOutlined />, [
        getItem('Team 1', '12'), 
        getItem('Team 2', '13')
    ]),
    getItem('Kemahasiswaan', 'sub6', <UsergroupAddOutlined />, [
        getItem('Team 1', '14'), 
        getItem('Team 2', '15')
    ]),
    getItem('Informasi', 'sub7', <InfoCircleOutlined />, [
        getItem('Team 1', '16'), 
        getItem('Team 2', '17')
    ]),
    getItem('Bursa kerja', '18', <ReconciliationOutlined />),
    getItem('Game Indonesia H.E.B.A.T', '19', <DesktopOutlined />),
  ];
  
  const SideBar = () => {
    const [collapsed, setCollapsed] = useState(false);

    const router = useRouter();

    const handleLogout = () => {
      router.push('/');
    }
    
    return (
        <Sider collapsed={collapsed} onCollapse={(value) => setCollapsed(value)} theme="light" style={{minWidth: '236px !important'}}>
              <div style={logoWrapper}> 
                  <Image width={50} className="pb-5" src={ImgLogo.src} />
                  <div style={verticalLine} />
                  <div className='text-left' style={titleWrapper}>
                      <p style={titleFont}>SISTEM LANGITAN</p>
                      <p style={titleFont}>MAHASISWA </p>
                  </div>
               </div>
               <Menu theme="light" defaultSelectedKeys={['1']} mode="inline" items={items} />
               <div style={btnWrapper}> 
                <Button size={'large'} style={btnStyle} type="submit" onClick={handleLogout} >Log out</Button>              
               </div>
               
        </Sider>
    );
  };
  
  export default SideBar;
  