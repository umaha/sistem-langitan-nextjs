import { Input } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

const rounded = { 
    borderRadius: '4px'
}

const InputField = ({ placeholder, name, icon, type, onChange, value, ...otherProps }) => { 
    return (
        <>
        {type === "password" ? (
            <Input.Password value={value} onChange={onChange} type={type}  name={name} size="large" placeholder={placeholder} style={rounded} iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
        ) : (
            <Input value={value} onChange={onChange} type={type} name={name} size="large" placeholder={placeholder} style={rounded} />
        )}
        </>
    );
}

InputField.prototype = {
    type: null,
    placeholder: null,
    name: null,
    onChange: null,
    value: '',
    icon: null,
}

export default InputField;