import React from 'react';
import PropTypes from 'prop-types';
import Sidebar from './Sidebar';
import { Layout, Breadcrumb } from 'antd';

const { Header, Content, Footer } = Layout;

// const Wrapper = styled.div`
//   background-color: #fafafc;
//   max-width: 1367px;
//   ${tw`w-full h-screen ml-auto mr-auto block`};
// `;

// const NavbarWrapper = styled.div`
//   ${tw`w-full`}
// `;

// const SidebarAndContent = styled.div`
//   height: calc(100% - 80px);
//   max-width: 1347px;
//   ${tw`flex ml-0 mr-auto`}
// `;
// const ContentWrapper = styled.div`
//   padding: 34px 0px 34px 34px;
//   ${tw`w-full h-full overflow-hidden hover:overflow-y-auto`};
//   margin-right: 14px;
//   &:hover {
//     margin-right: 0px;
//   }
//   &::-webkit-scrollbar {
//     width: 14px;
//     height: 14px;
//   }
//   &::-webkit-scrollbar-track {
//     background-color: transparent;
//   }
//   &::-webkit-scrollbar-thumb {
//     background-color: #cecece;
//     border-radius: 14px;
//     border: 4px solid rgba(0, 0, 0, 0);
//     background-clip: padding-box;
//   }
// `;

const wrapper = {
    display: 'flex',
    minHeight: '730px'
}

const contentWrapper = {
    padding: '32px',
    overflowY: 'scroll',
    maxHeight: '730px'
}

const sideBarWrapper = {
    overflow: 'hidden',
    maxHeight: '730px',
    width: '315px',
}

const sideBarContent = {
    overflow: 'auto',
    maxHeight: '730px',
    maxWidth: '315px',
}

const MainLayout = ({ children }) => {
  return (
    <Layout>
        <Layout style={wrapper}>
            <div style={sideBarWrapper}>
                <div style={sideBarContent}>
                    <Sidebar />
                </div>
            </div>
            <Content style={contentWrapper}>
                {children}
            </Content>
        </Layout>
    </Layout>
  );
};

MainLayout.propTypes = {
  children: PropTypes.any,
};

export default MainLayout;
